//
//  Math.swift
//  TestingStuffDemo
//
//  Created by James Cash on 07-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import Foundation

struct Math {

    static func naiveFib(_ n: Int) -> Int {
        if n == 1 { return 1 }
        if n == 2 { return 1 }
        return naiveFib(n-1) + naiveFib(n-2)
    }
    // 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89

    static var cache = [1: 1, 2: 1]
    static func memFib(_ n: Int) -> Int {
        if let f = cache[n] { return f }
        let newVal = memFib(n - 1) + memFib(n - 2)
        cache[n] = newVal
        return newVal
    }

    // lim n->infinity F[n] / F[n-1]

    static func closedFib(_ n: Int) -> Int {
        let phi = (1 - sqrt(5)) / 2
        let psi = 1 - phi
        return Int((pow(phi, Double(n)) - pow(psi, Double(n))) /
                    (phi - psi))
    }

    static let fib = closedFib
}
