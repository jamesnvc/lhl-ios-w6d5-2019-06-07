//
//  TestingStuffDemoUITests.swift
//  TestingStuffDemoUITests
//
//  Created by James Cash on 07-06-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import XCTest

class TestingStuffDemoUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
//        XCUIApplication().buttons["Next Screen"].tap()

        let app = XCUIApplication()
        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.staticTexts["screenNameLabel"]/*[[".staticTexts[\"Second Location\"]",".staticTexts[\"screenNameLabel\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.label,
                       "First Place")
        app/*@START_MENU_TOKEN@*/.buttons["nextPlaceButton"]/*[[".buttons[\"Go Onward\"]",".buttons[\"nextPlaceButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()

        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.staticTexts["Second Location"]/*[[".staticTexts[\"Second Location\"]",".staticTexts[\"screenNameLabel\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/.label,
                       "Second Location")
        app/*@START_MENU_TOKEN@*/.buttons["nextPlaceButton"]/*[[".buttons[\"Nothing\"]",".buttons[\"nextPlaceButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertEqual(app.staticTexts["Third Screen"].label,
                       "Third Screen")

        app/*@START_MENU_TOKEN@*/.buttons["nextPlaceButton"]/*[[".buttons[\"Nothing\"]",".buttons[\"nextPlaceButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssertEqual(app.staticTexts["Third Screen"].label,
                       "Third Screen")
        app.navigationBars["TestingStuffDemo.View"].buttons["Back"].tap()
        XCTAssertEqual(app/*@START_MENU_TOKEN@*/.staticTexts["Second Location"]/*[[".staticTexts[\"Second Location\"]",".staticTexts[\"screenNameLabel\"]"],[[[-1,1],[-1,0]]],[1]]@END_MENU_TOKEN@*/.label,
                       "Second Location")

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
